import { Component, NgModule, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DxTabsModule } from 'devextreme-angular';
import { HeaderTabItem } from 'src/app/models/headerTabItem.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  selectedTabIndex = 0;
  tabs: HeaderTabItem[] = [];

  constructor(
    private router: Router,
  ) {
    this.setTabs();
    this.setSelectedTabIndex();
  }

  ngOnInit(): void {
  }

  setTabs = () => {
    this.tabs = [
      new HeaderTabItem('fas fa-house-damage', 'Inicio', 'home'),
      new HeaderTabItem('fas fa-users', 'Personajes', 'characters'),
    ]
  }

  /**
   * Sets the selected tab index once the page has loaded
   */
  setSelectedTabIndex = () => {
    const currentPath = window.location.href;
    const currentRoute = currentPath.slice(currentPath.indexOf('#') + 2).toLowerCase();
    this.selectedTabIndex = this.tabs.findIndex(tab => tab.route === currentRoute)
  }

  /**
   * Change page once tab is clicked
   * @param e 
   */
  onTabClicked = (e: { itemData: HeaderTabItem }) => {
    const selectedTab: HeaderTabItem = e.itemData;
    this.router.navigateByUrl(`/${selectedTab.route}`);
  }
}

@NgModule({
  declarations: [
    HeaderComponent
  ],
  exports: [
    HeaderComponent
  ],
  imports: [
    DxTabsModule
  ]
}) export class HeaderModule { }