import { CommonModule } from '@angular/common';
import { Component, NgModule, OnInit } from '@angular/core';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-page-loader',
  templateUrl: './page-loader.component.html',
  styleUrls: ['./page-loader.component.scss']
})
export class PageLoaderComponent implements OnInit {

  showPageLoader = false;

  constructor(private sharedService : SharedService) { 
    this.sharedService.pageLoaderObservable.subscribe((showLoader:boolean) => {
      this.showPageLoader = showLoader
    })
  }

  ngOnInit(): void {
  }

}

@NgModule({
 declarations: [PageLoaderComponent],
 exports: [PageLoaderComponent],
 imports: [
   CommonModule
 ]
})export class PageLoaderModule {}