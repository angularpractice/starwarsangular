import { CommonModule } from '@angular/common';
import { Component, Input, NgModule, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() headerLeftText: string;
  @Input() headerRightText: string;
  @Input() image: string;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() description: string;

  @Output() cardClicked: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  onCardClicked = () => this.cardClicked.emit(false);
}

@NgModule({
  declarations: [CardComponent],
  exports: [CardComponent],
  imports: [
    CommonModule
  ]
}) export class CardModule { }
