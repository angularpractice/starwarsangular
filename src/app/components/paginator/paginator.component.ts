import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, NgModule, OnInit, Output } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {

  @Input() totalItems:number = 0;
  @Input() currentPage:number = 1;

  @Output() pageSelected: EventEmitter<number> = new EventEmitter<number>();

  pageSize = 10;

  constructor() { }

  ngOnInit(): void {
  }

  onPageSelected = (page:number) => this.pageSelected.emit(page)

}

@NgModule({
  declarations: [PaginatorComponent],
  exports: [PaginatorComponent],
  imports: [
    CommonModule,
    NgbModule
  ],
}) export class PaginatorModule { }