import { CommonModule } from '@angular/common';
import { Component, NgModule, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DxDataGridModule } from 'devextreme-angular';
import { Character } from 'src/app/models/character.model';
import { Film } from 'src/app/models/film.model';
import { CharactersService } from 'src/app/services/characters.service';
import { FilmService } from 'src/app/services/films.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.scss']
})
export class FilmComponent implements OnInit {

  filmId: number = null;
  film: Film = null;
  characters: Character[] = [];

  constructor(
    private route: ActivatedRoute,
    private filmService: FilmService,
    private characterService: CharactersService,
    private sharedService: SharedService
  ) { }

  ngOnInit(): void {
    this.filmId = parseInt(this.route.snapshot.paramMap.get('filmId'))
    this.getFilm();
  }

  getFilm = () => {
    this.sharedService.showLoader()
    this.filmService.getFilmById(this.filmId).subscribe((response: Film) => {
      this.sharedService.hideLoader()
      if (!response) {
        console.warn('Film could not be found')
      }
      this.film = response;
      this.getFilmCharacters()
    })
  }

  getFilmCharacters = async () => {
    if (this.film && this.film.characters.length > 0) {
      this.film.characters.forEach((characterResourceUrl: string) => {
        this.characterService.getCharacterByResourceUrl(characterResourceUrl).subscribe((response:Character) => {
          this.characters = [...this.characters, response]
        });
      })
    }
  }
}

@NgModule({
  declarations: [FilmComponent],
  exports: [FilmComponent],
  imports: [
    CommonModule,
    DxDataGridModule
  ]
}) export class FilmModule { }