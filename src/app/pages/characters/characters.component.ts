import { CommonModule } from '@angular/common';
import { Component, NgModule, OnInit } from '@angular/core';
import { CardModule } from 'src/app/components/card/card.component';
import { GridLayoutModule } from 'src/app/components/grid-layout/grid-layout.component';
import { PaginatorModule } from 'src/app/components/paginator/paginator.component';
import { Character } from 'src/app/models/character.model';
import { RequestResult } from 'src/app/models/requestResult.model';
import { CharactersService } from 'src/app/services/characters.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {

  currentPage = 1;
  characters: Character[] = [];
  totalItems = 0;
  imageURL = 'assets/images/characters/logo.jpg'

  constructor(
    private charactersService: CharactersService,
    private sharedService: SharedService
  ) {
    this.getAllCharacters();
   }

  ngOnInit(): void {
  }

  getAllCharacters = () => {
    this.sharedService.showLoader()
    this.charactersService.getAllCharacters(this.currentPage).subscribe((response: RequestResult) => {
      this.sharedService.hideLoader()
      if(!response){
        return console.warn('Error al consumir servicio');
      }
      this.characters = response.results;
      this.totalItems = response.count;
    })
  }

  onPageSelected = (page:number) => {
    this.currentPage = page;
    this.getAllCharacters();
  }
}

@NgModule({
  declarations:[
    CharactersComponent
  ],
  exports: [
    CharactersComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    GridLayoutModule,
    PaginatorModule
  ],
})export class CharacterModule {}
