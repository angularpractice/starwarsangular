import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Film } from 'src/app/models/film.model';

@Component({
  selector: 'app-film-summary-modal',
  templateUrl: './film-summary-modal.component.html',
  styleUrls: ['./film-summary-modal.component.scss']
})
export class FilmSummaryModalComponent implements OnInit {

  @Input() film: Film;
  @Input() showModal: boolean = false;
  @Output() closeClicked: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  onCloseClicked = () => this.closeClicked.emit(false);

}
