import { CommonModule } from '@angular/common';
import { Component, NgModule, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DxPopupModule } from 'devextreme-angular';
import { CardModule } from 'src/app/components/card/card.component';
import { GridLayoutModule } from 'src/app/components/grid-layout/grid-layout.component';
import { PaginatorModule } from 'src/app/components/paginator/paginator.component';
import { Film } from 'src/app/models/film.model';
import { RequestResult } from 'src/app/models/requestResult.model';
import { FilmService } from 'src/app/services/films.service';
import { SharedService } from 'src/app/services/shared.service';
import { FilmSummaryModalComponent } from './film-summary-modal/film-summary-modal.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  currentPage = 1;
  totalItems = 0;
  films: Film[] = []
  selectedFilm: Film = null;
  showSummaryModal = false;

  constructor(
    private filmService: FilmService,
    private router: Router,
    private sharedService: SharedService
  ) {
    this.getAllFilms();
  }

  ngOnInit(): void {
  }

  getAllFilms = () => {
    this.sharedService.showLoader()
    this.filmService.getAllFilms(this.currentPage).subscribe((response: RequestResult) => {
      this.sharedService.hideLoader()
      if (!response) {
        return console.warn('Error while getting films')
      }
      this.films = response.results;
      this.totalItems = response.count;
      // Assign each film's image
      this.films.forEach(film => {
        film.image = `assets/images/films/${film.episode_id}.jpg`
      });
    })
  }

  onFilmSelected = (film: Film) => this.selectedFilm = film;

  onCloseSummaryModal = () => this.showSummaryModal = false;

  onPageSelected = (page: number) => {
    this.currentPage = page;
    this.getAllFilms();
  }

  onSummaryClicked = (film: Film) => {
    this.selectedFilm = film;
    this.showSummaryModal = true;
  }

  onCastClicked = (film: Film) => this.router.navigate(['/film', film.episode_id])
}

@NgModule({
  declarations: [
    HomeComponent,
    FilmSummaryModalComponent
  ],
  exports: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    GridLayoutModule,
    PaginatorModule,
    NgbModule,
    DxPopupModule
  ],
}) export class HomeModule { }