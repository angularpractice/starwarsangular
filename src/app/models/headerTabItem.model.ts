import { v4 as uuid } from 'uuid';

export class HeaderTabItem {
    id: string;
    icon: string;
    text: string;
    route: string;

    constructor(icon:string, text:string, route:string){
        this.id = uuid();
        this.icon = icon;
        this.text = text;
        this.route = route
    }
}