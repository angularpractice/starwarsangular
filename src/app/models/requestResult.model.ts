export class RequestResult {
    count: number;
    next: string;
    previous: string;
    results: any;
}