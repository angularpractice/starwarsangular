import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

class Config {
  apiUrl: string;
}


@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  configJsonPath = "./assets/config.json"

  constructor(private httpClient: HttpClient) {
  }

  /**
   * Sets the data from the config.json file in the local storage
   */
  setConfigInLS = () => {
    this.getConfigJsonData().subscribe((data: Config) => {
      if (!data) {
        return console.warn('The config file does not have any data')
      }
      // Check if the local storage already has the base url
      localStorage.setItem("config", JSON.stringify(data));
    });
  }


  /**
   * Gets the data from the config.json file
   */
  getConfigJsonData = (): Observable<any> => {
    return this.httpClient.get(this.configJsonPath);
  }

  /**
   * Gets the api base url
   */
  getBaseUrl = (): string => {
    let config: Config = JSON.parse(localStorage.getItem("config"));
    return config.apiUrl;
  }

}
