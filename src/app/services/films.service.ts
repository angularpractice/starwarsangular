import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  constructor(
    private httpClient: HttpClient,
    private configService: ConfigService
  ) { }

  /**
   * Gets all fimls according to page
   * @param page 
   */
  getAllFilms = (page:number) => {
    return this.httpClient.get(`${this.configService.getBaseUrl()}films/?page=${page}`)
  }

  /**
   * Gets film by id
   * @param filmId 
   */
  getFilmById = (filmId: number) => {
    return this.httpClient.get(`${this.configService.getBaseUrl()}films/${filmId}`)
  }
}
