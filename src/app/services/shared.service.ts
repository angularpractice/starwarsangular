import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  pageLoaderObservable: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() { }

  private toggleLoader = (showLoader:boolean) => this.pageLoaderObservable.next(showLoader);

  showLoader = () => this.toggleLoader(true);

  hideLoader = () => this.toggleLoader(false);
}
