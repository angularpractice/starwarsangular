import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class CharactersService {

  constructor(
    private httpService: HttpClient,
    private configService: ConfigService
  ) { }

  /**
   * Gets all characters according to page
   * @param page 
   */
  getAllCharacters = (page: number) => {
    return this.httpService.get(`${this.configService.getBaseUrl()}people/?page=${page}`)
  }

  /**
   * Gets character by resource url
   * @param resourceURL 
   */
  getCharacterByResourceUrl = (resourceURL: string) => {
    return this.httpService.get(resourceURL);
  }
}
